#ifndef OMCPPUSHKA_HASHMAP_H
#define OMCPPUSHKA_HASHMAP_H


#include <iostream>

template < typename K, typename V >
class HashMap {
private:
    template < typename KE, typename VE >
    struct HashNode {
        K key;
        V value;
        int hash;
        HashNode* next;
    };

    template < typename KI, typename VI >
    class Itr {
    private:
        int cursor;
        HashMap<KI, VI>* map;
        HashNode<KI, VI>* node;
    public:
        Itr(HashMap<KI, VI> *map) {
            this->map = map;
        }

        void start() {
            cursor = 0;
            while (map->table[cursor] == nullptr && cursor < map->capacity)
                cursor++;
            if (cursor == map->capacity) {
                cursor = 0;
                node = nullptr;
            } else
                node = map->table[cursor];
        }

        bool hasNext() {
            if (node == nullptr)
                return false;
            if (node->next != nullptr)
                return true;
            else {
                int temp = cursor + 1;
                while (map->table[temp] == nullptr && temp < map->capacity)
                    temp++;
                return temp != map->capacity;
            }
        }

        void next() {
            if (node->next != nullptr) {
                node = node->next;
            } else {
                cursor++;
                while (map->table[cursor] == nullptr && cursor < map->capacity)
                    cursor++;
                if (cursor == map->capacity) {
                    cursor = 0;
                    node = nullptr;
                } else
                    node = map->table[cursor];
            }
        }

        VI getValue() {
            return node->value;
        }
    };

    HashNode<K, V>** table;         // hash table with chains
    int capacity;                   // maximal capacity
    int size;                       // current size
    const double loadFactor = 0.75; // loadfactor for resizing if table is full enough
    int (* hash)(K);                 // hash for elements
    bool (* areEqual)(K, K);          // boolean for comparing keys

    void resize(int newCapacity) {
        HashNode<K, V>** newTable = new HashNode<K, V>* [newCapacity];
        for (int i = 0; i < capacity; i++)
            newTable[i] = table[i];
        for (int j = capacity; j < newCapacity; ++j) {
            newTable[j] = nullptr;
        }
        for (int k = 0; k < capacity; ++k) {
            HashNode<K,V> *entry = newTable[k];
            HashNode<K,V> *previous = nullptr;
            HashNode<K,V> *next = nullptr;
            while (entry != nullptr) {
                next = entry->next;
                int keyHash = entry->hash;
                int pos = keyHash % newCapacity;
                if (pos != k) {
                    if (previous != nullptr)
                        previous->next = entry->next;
                    else
                        newTable[k] = entry->next;
                    entry->next = newTable[pos];
                    newTable[pos] = entry;
                } else
                    previous = entry;
                entry = next;
            }
        }
        delete[] table;
        table = newTable;
        capacity = newCapacity;
    }


public:
    HashMap(int(* hash)(K), bool(* areEqual)(K, K), int capacity) {
        this->capacity = capacity;
        size = 0;
        this->hash = hash;
        this->areEqual = areEqual;
        table = new HashNode<K, V>* [capacity];
        for (int i = 0; i < capacity; ++i) {
            table[i] = nullptr;
        }
    };

    V* put(K key, V value) {
        int keyHash = hash(key);
        int pos = keyHash % capacity;
        HashNode<K, V>* entry = table[pos];
        HashNode<K, V>* previous = nullptr;
        while (entry != nullptr) {
            if (keyHash == entry->hash && areEqual(key, entry->key)) {
                V res = entry->value;
                entry->value = value;
                return &res;
            }
            previous = entry;
            entry = entry->next;
        }
        if (size > capacity * loadFactor)
            resize(size+capacity);
        entry = new HashNode<K, V>;
        entry->value = value;
        entry->key = key;
        entry->next = nullptr;
        if (previous != nullptr)
            previous->next = entry;
        else table[pos] = entry;
        size++;
        return &value;
    };

    V* get(K key) {
        int keyHash = hash(key);
        int pos = keyHash % capacity;
        HashNode<K, V>* entry = table[pos];
        while (entry != nullptr) {
            if (keyHash == entry->hash && areEqual(key, entry->key)) {
                return &entry->value;
            }
            entry = entry->next;
        }
        return nullptr;
    };

    V* remove(K key) {
        int keyHash = hash(key);
        int pos = keyHash % capacity;
        HashNode<K, V>* entry = table[pos];
        HashNode<K, V>* previous = nullptr;
        while (entry != nullptr) {
            if (keyHash == entry->hash && areEqual(key, entry->key)) {
                V res = entry->value;
                if (previous != nullptr) {
                    previous->next = entry->next;
                } else table[pos] = entry->next;
                delete entry;
                size--;
                return &res;
            }
            previous = entry;
            entry = entry->next;
        }
        return nullptr;
    }

    bool isEmpty() {
        return size == 0;
    }

    void doEmpty() {
        for (int i = 0; i < capacity; ++i) {
            HashNode<K, V>* entry = table[i];
            HashNode<K, V>* next = nullptr;
            while (entry != nullptr) {
                next = entry->next;
                delete entry;
                entry = next;
            }
        }
        size = 0;
    }

    Itr<K, V>* iterator() {
        return new Itr<K, V>(this);
    }

    void print() {
        auto itr = iterator();
        itr->start();
        if (size > 0)
            std::cout << itr->getValue() << " ";
        while (itr->hasNext()) {
            itr->next();
            std::cout << itr->getValue() << " ";
        }
    }
};

#endif //OMCPPUSHKA_HASHMAP_H
