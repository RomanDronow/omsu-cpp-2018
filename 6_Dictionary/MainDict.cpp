#include "Dictionary.h"
#include <iostream>
#include <string>
using namespace std;

int main6(){
    Dictionary dict;
    std::string a = "Roman";
    std::string b = "Dronov";
    std::string c = "OmSU";
    std::string d = "IMIT";
    std::string e = "Course";
    dict.insert(a); //Roman1
    dict.insert(a); //Roman2
    dict.insert(b); //Dronov1
    dict.insert(a); //Roman3
    dict.insert(e); //Course1
    dict.insert(c); //OmSU1
    dict.insert(b); //Dronov2
    dict.insert(d); //IMIT1
    dict.insert(d); //IMIT2
    dict.insert(e); //Course2
    cout << dict;
    cout << "\nWords count: " << dict.countWords() << "\n\n";
    cout << "Roman here is " << dict.search(a) << " times\n\n";
    dict.remove(a);
    dict.remove(a);
    cout << "Deleted 2 Roman\n" << dict << "\n";
    dict.remove(c);
    cout << "Deleted OmSU\n"<< dict;

}