#ifndef OMCPPUSHKA_DICTIONARY_H
#define OMCPPUSHKA_DICTIONARY_H

#include <string>
#include <ostream>

class Dictionary {
    class Node {
    public:
        std::string word;
        int count;
        Node* left;
        Node* right;

        Node(std::string wrd, int c, Node* left, Node* right) {
            word = wrd;
            count = c;
            this->left = left;
            this->right = right;
        }
    };

public:
    Dictionary() {
        root = nullptr;
    };

    ~Dictionary() {
        clear();
    };

    Dictionary &operator=(const Dictionary &other) {
        copy(other);
        return *this;
    };

    Dictionary &operator=(Dictionary &&other) {
        if (this == &other) {
            return *this;
        }
        root = other.root;
        other.root = nullptr;
    };

    int search(std::string &wrd) {
        return search(wrd, root);
    };

    void insert(std::string &wrd) {
        insert(wrd, root);
    };

    int countWords() {
        return countWords(root);
    };

    void remove(std::string &wrd) {
        remove(wrd, root, nullptr);
    };

    void clear() {
        clear(root);
    };

    void copy(const Dictionary &other) {
        if (this == &other) {
            return;
        }
        clear();
        root = copy(other.root);
    };

    friend std::ostream& operator<<(std::ostream &os, Dictionary &dict) {
       dict.print(os, dict.root);
       return os;
    };

private:
    Node* root;

    Node* copy(Node* node) {
        if (!root)
            return nullptr;
        Node* tmp = new Node(root->word, root->count, copy(root->left), copy(root->right));
        return tmp;
    };

    void clear(Node*&node) {
        if (node == nullptr) {
            return;
        }
        clear(node->left);
        clear(node->right);
        delete node;
        node = nullptr;
    };

    int search(std::string &wrd, Node* node) {
        if (!node)
            return 0;
        if (node->word == wrd)
            return node->count;
        return search(wrd, node->left) + search(wrd, node->right);
    };

    void insert(std::string &wrd, Node*& node) {
        if (!node) {
            node = new Node(wrd, 1, nullptr, nullptr);
            return;
        }
        if (node->word == wrd) {
            node->count++;
            return;
        }
        if (wrd < node->word)
            insert(wrd, node->left);
        else insert(wrd, node->right);
    };

    int countWords(Node* node) {
        if (!node)
            return 0;
        return node->count + countWords(node->left) + countWords(node->right);
    };

    bool remove(std::string wrd, Node* node, Node* prev) {
        if (wrd > node->word)
            return remove(wrd, node->right, node);
        if (wrd < node->word)
            return remove(wrd, node->left, node);
        if (node->word == wrd) {
            if (node->count > 1) {
                node->count--;
                return true;
            } else {
                if (node->left && node->right) {
                    Node* tmp = rightNodeInLeftSubtreeParent(node);
                    node->word = tmp->right->word;
                    node->count = tmp->right->count;
                    tmp->right = tmp->right->left;
                } else if (node->left) {
                    if (prev->left == node)
                        prev->left = node->left;
                    else
                        prev->right = node->left;
                } else if (node->right) {
                    if (prev->left == node)
                        prev->left = node->right;
                    if (prev->right == node)
                        prev->right = node->right;
                } else {
                    if(prev->left == node)
                        prev->left = nullptr;
                    else
                        prev->right = nullptr;
                }
                return true;
            }
        }
    };

    Node* rightNodeInLeftSubtreeParent(Node* node) {
        Node* prev = nullptr;
        while (node->right != nullptr) {
            prev = node;
            node = node->right;
        }
        return prev;
    }

    void print(std::ostream& os, Node* node){
        if(!node)
            return;
        print(os, node->left);
        os << "Word: " << node->word << "[" << node->count << "]\n";
        print(os, node->right);
    }
};


#endif //OMCPPUSHKA_DICTIONARY_H
