#include "List.h"
#include <iostream>

template < class T >
void print(List<T> list) {
	AbstractIterator<T>* itr = list.iterator();
	itr->start();
	itr->start();
	while (!itr->isFinished()) {
		std::cout << itr->getValue() << " ";
		itr->next();
	}
	std::cout << "\n";
}

int main3() {
	List<int> list = List<int>();
	if (list.isEmpty()) {
		std::cout << "true";
	}
	else {
		std::cout << "false";
	}
	std::cout << "\n";	
	auto itr = list.iterator();
	list.insert(itr, 10);
	list.insert(itr, 12);						
	std::cout << list.getSize() << "\n";
	list.insert(itr, 6);
	std::cout << list.getSize() << "\n";
	itr->start();
	list.remove(itr);
	print(list);

	itr->start();
	list.insert(itr, 12);
	list.insert(itr, 6); 
	list.insert(itr, 5); 
	list.insert(itr, 13);

	print(list);

	itr = list.findFirst(5);
	list.remove(itr);
	print(list);

	system("pause");
	return 666;
}

