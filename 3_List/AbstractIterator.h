#ifndef OMCPPUSHKA_ABSTRACTITERATOR_H
#define OMCPPUSHKA_ABSTRACTITERATOR_H

#include "Node.h"

template<class T>
class AbstractIterator {
public:
	virtual void start() = 0;

	virtual T getValue() = 0;

	virtual void next() = 0;

	virtual bool isFinished() = 0;

	virtual Node<T> *getNode() = 0;
};


#endif //OMCPPUSHKA_ABSTRACTITERATOR_H
