#ifndef OMCPPUSHKA_LIST_H
#define OMCPPUSHKA_LIST_H


#include <type_traits>
#include "AbstractList.h"

template<class T>
class List : public AbstractList<T> {
private:

	template<class TI>
	class Itr : public AbstractIterator<T> {
	private:
		List * list;
		Node<TI> *cursor;
		bool finished;
	public:

		explicit Itr(List<TI> *list1) {
			this->list = list1;
			finished = false;
			cursor = list->buffer;
		}

		void start() override {
			cursor = list->buffer->next;
		}

		T getValue() override {
			return cursor->value;
		}

		void next() override {
			cursor = cursor->next;
			if (cursor == list->buffer) {
				cursor = cursor->next;
				finished = true;
			}
		}

		bool isFinished() override {
			return finished;
		}

		Node<TI> *getNode() override {
			return cursor;
		}
	};

	Node<T> *buffer;
	int size;

public:

	List();

	void insert(AbstractIterator<T> *iter, T elem);

	void remove(AbstractIterator<T> *iter);

	AbstractIterator<T> *findFirst(T elem);

	void doEmpty();

	bool isEmpty();

	int getSize();

	AbstractIterator<T> *iterator();
};

template<class T>
int List<T>::getSize() {
	return size;
}

template<class T>
bool List<T>::isEmpty() {
	return size == 0;
}

template<class T>
void List<T>::doEmpty() {
	Node<T> *current, *next = buffer->next;
	for (int i = 0; i < size; i++) {
		current = next;
		next = current->next;
		delete current;
	}
	buffer->next = buffer;
	buffer->prev = buffer;
	size = 0;

}

template<class T>
void List<T>::insert(AbstractIterator<T> *iter, T elem) {
	Node<T> *node = new Node<T>();
	node->value = elem;
	Node<T> *current = iter->getNode();
	Node<T> *next = current->next;
	current->next = node;
	node->prev = current;
	next->prev = node;
	node->next = next;
	size++;
}

template<class T>
AbstractIterator<T> *List<T>::findFirst(T elem) {
	AbstractIterator<T> *iter = iterator();
	iter->start();
	while (!iter->isFinished()) {
		if (iter->getValue() == elem)
			return iter;
		iter->next();
	}
	return iterator();
}

template<class T>
void List<T>::remove(AbstractIterator<T> *iter) {
	Node<T> *current = iter->getNode();
	if (current == buffer)
		throw "Can't remove buffer element.";
	Node<T> *next = current->next;
	Node<T> *prev = current->prev;
	next->prev = prev;
	prev->next = next;
	size--;
	delete current;
}

template<class T>
AbstractIterator<T> *List<T>::iterator() {
	return new Itr<T>(this);
}

template<class T>
List<T>::List() {
	buffer = new Node<T>();
	buffer->next = buffer;
	buffer->prev = buffer;
	size = 0;
}


#endif //OMCPPUSHKA_LIST_H
