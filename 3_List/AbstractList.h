#ifndef OMCPPUSHKA_ABSTRACTLIST_H
#define OMCPPUSHKA_ABSTRACTLIST_H


#include "AbstractIterator.h"

template<class T>
class AbstractList {
public:
	virtual void insert(AbstractIterator<T> *iter, T elem) = 0;

	virtual void remove(AbstractIterator<T> *iter) = 0;

	virtual AbstractIterator<T> *findFirst(T elem) = 0;

	virtual void doEmpty() = 0;

	virtual bool isEmpty() = 0;

	virtual int getSize() = 0;

	virtual AbstractIterator<T> *iterator() = 0;
};


#endif //OMCPPUSHKA_ABSTRACTLIST_H
