#ifndef OMCPPUSHKA_NODE_H
#define OMCPPUSHKA_NODE_H

template<class T>
class Node {
public:
	T value;
	Node<T> *next;
	Node<T> *prev;
};


#endif //OMCPPUSHKA_NODE_H
