#include "DynArray.h"

DynArray::DynArray() {
	size = 8;
	data = new int[size];
	for (int i = 0; i < size; ++i) {
		data[i] = 0;
	}
}

DynArray::DynArray(int paramSize) {
	size = paramSize;
	data = new int[paramSize];
	for (int i = 0; i < size; ++i) {
		data[i] = 0;
	}
}

DynArray::DynArray(int paramSize, int value) {
	size = paramSize;
	data = new int[paramSize];
	for (int i = 0; i < size; ++i) {
		data[i] = value;
	}
}

DynArray::DynArray(const DynArray &other) {
	size = other.size;
	data = new int[size];
	for (int i = 0; i < size; ++i) {
		data[i] = other.data[i];
	}
}

DynArray::DynArray(DynArray &&other) noexcept {
	size = other.size;
	data = other.data;
	other.data = nullptr;
}

DynArray::~DynArray() {
	delete[] data;
}

int DynArray::getSize() {
	return size;
}

int DynArray::operator[](int element) const {
	return data[element];
}

int &DynArray::operator[](int element) {
	return data[element];
}

DynArray DynArray::resize(int newSize) {
	int *newData = new int[newSize];
	if (newSize >= size) {
		for (int i = 0; i < size; i++) {
			newData[i] = data[i];
		}
		for (int k = size; k < newSize; k++) {
			newData[k] = 0;
		}
	}
	if (newSize < size) {
		for (int j = 0; j < newSize; j++) {
			newData[j] = data[j];
		}
	}
	size = newSize;
	data = newData;
	return *this;

}

DynArray DynArray::operator=(const DynArray &other) {
	if (this != &other) {
		delete[] data;
		size = other.size;
		data = new int[size];
		for (int i = 0; i < size; i++) {
			data[i] = other.data[i];
		}
	}
	return *this;
}

DynArray DynArray::operator=(DynArray &&other) noexcept {
	if (this != &other) {
		delete[] data;
		data = other.data;
		size = other.size;
		other.size = 0;
		other.data = nullptr;
	}
	return *this;
}

bool DynArray::operator==(const DynArray &other) {
	if (size != other.size) {
		return false;
	}
	for (int i = 0; i < size; ++i) {
		if (data[i] != other.data[i]) {
			return false;
		}
	}
	return true;
}

bool DynArray::operator!=(const DynArray &other) {
	return !(*this == other);
}

bool DynArray::operator<(const DynArray &other) {
	if (this == &other) {
		return false;
	}
	int minSize = std::min(this->size, other.size);
	for (int i = 0; i < minSize; i++) {
		if (this->data[i] > other.data[i]) {
			return false;
		}
	}
	for (int i = 0; i < minSize; i++) {

		if (this->data[i] < other.data[i]) {
			return true;
		}
	}
}

bool DynArray::operator>(const DynArray &other) {
	if (this == &other) {
		return false;
	}
	int minSize = std::min(this->size, other.size);
	for (int i = 0; i < minSize; i++) {
		if (this->data[i] < other.data[i]) {
			return false;
		}
	}
	for (int i = 0; i < minSize; i++) {

		if (this->data[i] > other.data[i]) {
			return true;
		}
	}
}

bool DynArray::operator<=(const DynArray &other) {
	if (this == &other) {
		return true;
	}
	int minSize = std::min(this->size, other.size);
	for (int i = 0; i < minSize; i++) {
		if (this->data[i] > other.data[i]) {
			return false;
		}
	}
	for (int i = 0; i < minSize; i++) {

		if (this->data[i] < other.data[i]) {
			return true;
		}
	}
}

bool DynArray::operator>=(const DynArray &other) {
	if (this == &other) {
		return true;
	}
	int minSize = std::min(this->size, other.size);
	for (int i = 0; i < minSize; i++) {
		if (this->data[i] < other.data[i]) {
			return false;
		}
	}
	for (int i = 0; i < minSize; i++) {

		if (this->data[i] > other.data[i]) {
			return true;
		}
	}
}

DynArray DynArray::operator+(const DynArray &other) {
	int newSize = size + other.size;
	int* newData = new int[newSize];
	for (int i = 0; i < size; i++) {
		newData[i] = data[i];
	}
	for (int j = 0; j < other.size; j++) {
		newData[j + size] = other.data[j];
	}
	size = newSize;
	data = newData;
	return *this;
}

std::ostream &operator<<(std::ostream &os, const DynArray &array) {

	for (int i = 0; i < array.size; i++) {
		std::cout << array.data[i] << " ";
	}
	std::cout << std::endl;
	return os;
}

std::istream &operator>>(std::istream &is, const DynArray &array) {
	for (int i = 0; i < array.size; i++) {
		is >> array.data[i];
	}
	return is;
}

