#include "DynArray.h"

using namespace std;

int main1() {
	DynArray da1 = DynArray();  // �� ���������

	DynArray da2 = DynArray(5); // �� �������

	da2[1] = 4000000;                 // �������-���

	DynArray da3 = da2;   // �����������

	cout << da1.getSize() << endl << da3[1] << endl; // ������ � �������-���

	da2.resize(7); // ��������� ������

	DynArray da4 = da2; // ����������� ��������

	if (da4 == da2) {
		cout << "true" << endl;     //  ����� ���
	}
	else cout << "false" << endl;

	if (da3 == da2) {
		cout << "true" << endl;     // ����� ����
	}
	else cout << "false" << endl;

	if (da1 != da2) {
		cout << "true" << endl;     //  ������� ���
	}
	else cout << "false" << endl;

	if (da4 != da2) {
		cout << "true" << endl;     // ������� ����
	}
	else cout << "false" << endl;

	DynArray ls = DynArray(3);
	ls[0] = 3;
	ls[1] = 2;
	ls[2] = 5;
	DynArray rs = DynArray(2);  // ��������
	rs[0] = 11;
	rs[1] = 14;
	cout << ls + rs;

	DynArray inp = DynArray(4);     // ����
	cin >> inp;
	cout << inp;

	DynArray one(2);
	one[0] = 1;
	one[1] = 4;
	DynArray two(2);
	two[0] = 1;
	two[1] = 5;

	if (one < two) {
		cout << "true" << endl;     // ������ ���
	}
	else cout << "false" << endl;

	system("pause");

	return 666;
}