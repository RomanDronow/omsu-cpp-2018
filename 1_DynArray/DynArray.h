#ifndef OMCPPUSHKA_DYNARRAY_H
#define OMCPPUSHKA_DYNARRAY_H

#include <iostream>
#include <algorithm>

class DynArray {
private:
	int size;
	int *data;

public:
	DynArray();

	explicit DynArray(int paramSize);

	DynArray(int paramSize, int value);

	DynArray(const DynArray &other);

	DynArray(DynArray &&other) noexcept;

	~DynArray();

	int getSize();

	int operator[](int element) const;

	int& operator[](int element);

	DynArray resize(int newSize);

	DynArray operator=(const DynArray &other);

	DynArray operator=(DynArray &&other) noexcept;

	bool operator==(const DynArray &other);

	bool operator!=(const DynArray &other);

	bool operator<(const DynArray &other);

	bool operator>(const DynArray &other);

	bool operator<=(const DynArray &other);

	bool operator>=(const DynArray &other);

	DynArray operator+(const DynArray &other);

	friend std::ostream &operator<<(std::ostream &os, const DynArray &array);

	friend std::istream &operator>>(std::istream &is, const DynArray &array);

};


#endif //OMCPPUSHKA_DYNARRAY_H
