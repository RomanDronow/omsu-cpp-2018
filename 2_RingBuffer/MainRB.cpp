#include "RingBuffer.h"
#include <iostream>

int main2() {
    RingBuffer<int> rb = RingBuffer<int>(3);
    rb.push(4);
    rb.push(5);
    rb.push(7);
    auto itr = rb.iterator();
    while (!itr.finish()) {
        std::cout << itr.getValue() << " ";
        itr.next();
    }
    std::cout << "\n";

    if (rb.isEmpty()) {
        std::cout << "true\n";
    } else {
        std::cout << "false\n";
    }
    rb.doEmpty();
    if (rb.isEmpty()) {
        std::cout << "true\n";
    } else {
        std::cout << "false\n";
    }


    RingBuffer<double> rb2 = RingBuffer<double>(4);
    rb2.push(5.4);
    rb2.push(4.1);
    rb2.push(2.7);
    rb2.push(11.1);
    auto itr2 = rb2.iterator();
    itr2.start();
    while (!itr2.finish()) {
        std::cout << itr2.getValue() << " ";
        itr2.next();
    }

    std::cout << "\n";

    std::cout << rb2.peek() << " ";
    std::cout << rb2.peek() << " ";
    std::cout << rb2.peek() << " ";
    std::cout << rb2.peek() << "\n";
    std::cout << rb2.getSize() << " " << rb2.getCapacity() << "\n";

    std::cout << rb2.pop() << " ";
    std::cout << rb2.pop() << " ";
    std::cout << rb2.pop() << " ";
    std::cout << rb2.pop() << "\n";
    std::cout << rb2.getSize() << " " << rb2.getCapacity() << "\n";
    system("pause");
    return 666;
}