#ifndef OMCPPUSHKA_RINGBUFFER_H
#define OMCPPUSHKA_RINGBUFFER_H

template < typename T >
class RingBuffer {
private:

	T * buffer;
	int head;
	int tail;
	int size;
	int capacity;

	class Iterator {
	public:
		Iterator(RingBuffer<T> &queue) {
			this->queue = &queue;
			current = 0;
			count = 0;
		}
		~Iterator() {
			current = -1;
			queue = nullptr;
			count = 0;
		}
		void start() {
			current = queue->head;
		}
		void next() {
			current = (current + 1) % queue->capacity;
			count++;
		}
		bool finish() {
			return count == queue->size;
		}
		T getValue() {
			return queue->buffer[current];
		}
	private:
		RingBuffer * queue;
		int current;
		int count;
	};

public:

	explicit RingBuffer(int);

	void push(T element);

	T pop();

	T peek();

	int getSize();

	int getCapacity();

	void doEmpty();

	bool isEmpty();

	Iterator iterator() {
		return Iterator(*this);
	}
};

template < class T >
RingBuffer<T>::RingBuffer(int capacity) {
	this->capacity = capacity;
	buffer = new T[capacity];
	head = 0;
	tail = -1;
	size = 0;
}

template < class T >
void RingBuffer<T>::push(T element) {
	if (size == capacity) {
		throw "Buffer overflow";
	}
	if (tail == capacity - 1) {
		tail = -1;
	}
	buffer[++tail] = element;
	size++;
}

template < class T >
T RingBuffer<T>::pop() {
	if (isEmpty())
		throw "No element to pop";
	size--;
	if (head != capacity - 1)
		return buffer[head++];
	else {
		head = 0;
		return buffer[capacity - 1];
	}
}

template < class T >
T RingBuffer<T>::peek() {
	if (isEmpty())
		throw "Nothing to peek";
	return buffer[head];
}

template < class T >
int RingBuffer<T>::getSize() {
	return size;
}

template < class T >
int RingBuffer<T>::getCapacity() {
	return capacity;
}

template < class T >
bool RingBuffer<T>::isEmpty() {
	return size == 0;
}

template < class T >
void RingBuffer<T>::doEmpty() {
	buffer = new T[capacity];
	head = 0;
	tail = -1;
	size = 0;
}


#endif //OMCPPUSHKA_RINGBUFFER_H
