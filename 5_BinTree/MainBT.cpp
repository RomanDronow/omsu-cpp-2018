#include "BinaryTree.h"
#include <iostream>

using namespace std;

int main5(){
    BinaryTree bt;
    bt.insert(50, "");
    bt.insert(20, "0");
    bt.insert(10, "00");
    bt.insert(33, "01");
    bt.insert(70, "1");
    bt.insert(100, "11");
    bt.insert(60, "10");

    cout << bt << "\n";
    cout << "1, if all values are positive, 0 else = " << bt.arePositives() << "\n";
    cout << "even = " << bt.countEven() << "\n";
    cout << "average = " << bt.average() << "\n";

    auto path = bt.find(33);
    cout << "path to 33: ";
    for(char c : path)
        cout << c;
    cout << "\n";
    path = bt.find(49);
    cout << "path to 49: ";
    for (char c : path)
        cout << c;

    cout << "\n";
    bt.removeLeaves();
    cout << bt << "\n";
    bt.removeLeaves();
    cout << bt << "\n";
    bt.removeLeaves();
    cout << bt << "\n";
    bt.removeLeaves();
    cout << bt << "\n";
}