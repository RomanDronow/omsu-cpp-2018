#ifndef OMCPPUSHKA_BINARYTREE_H
#define OMCPPUSHKA_BINARYTREE_H

#include <cstring>
#include <ostream>
#include <vector>

class BinaryTree {

    class Node {
    public:
        int value;
        Node* left;
        Node* right;

        explicit Node(int x) {
            value = x;
            left = nullptr;
            right = nullptr;
        }
    };

public:

    Node* root;

    BinaryTree() {
        root = nullptr;

    }; // конструктор

    ~BinaryTree() {
        destroyTree(root);
    };  // деструктор вызывает уничтожение дерева

    BinaryTree &operator=(const BinaryTree &other) {
        if (this != &other) {
            destroyTree(root);
            root = new Node(other.root->value);
            copy(other.root, root);
        }
    }

    void insert(int x, char* path) {
        Node* current = root;
        Node* tmp = nullptr;
        for (int i = 0; i < strlen(path); i++) {
            tmp = current;
            if (current == nullptr) {
                throw "Path error";
            }
            if (path[i] == '0') {
                current = current->left;
            } else if (path[i] == '1') {
                current = current->right;
            } else throw "Path error";
        }
        if (current == nullptr) {
            if (tmp == nullptr) {
                root = new Node(x);
            } else {
                current = new Node(x);
                path[strlen(path) - 1] == '0' ? tmp->left = current : tmp->right = current;
            }
        } else current->value = x;
    };

    friend std::ostream &operator<<(std::ostream &os, const BinaryTree &tree) {
        return print(os, tree.root, 0);
    } // вывод в поток

    int countEven(){
        return countEven(root);
    }

    bool arePositives(){
        return arePositives(root);
    }

    void removeLeaves(){
        if (removeLeaves(root))
            root = nullptr;
    }

    double average() {
        int sum = 0;
        int count = 0;
        average(root, sum, count);
        if (count == 0)
            return 0;
        return (double) sum / count;
    }

    std::vector<char> find(int elem) {
        std::vector<char> res;
        if (find(res, root, elem)) {
            res.pop_back();
            return res;
        } else {
            res.clear();
            res.push_back('n');
            return res;
        }
    }

private:

    void destroyTree(Node* node) {
        if (!node)
            return;
        destroyTree(node->left);
        destroyTree(node->right);
        node->left = nullptr;
        node->right = nullptr;
        delete node;
    }; // уничтожение дерева

    void copy(Node* src, Node* dest) {
        if (!src)
            return;
        if (src->left) {
            dest->left = new Node(src->left->value);
            copy(src->left, dest->left);
        }
        if (src->right) {
            dest->right = new Node(src->right->value);
            copy(src->right, dest->right);
        }
    }

    static std::ostream &print(std::ostream &os, const Node* node, int row){
        if(node == nullptr)
            return os;
        for(int i = 0; i < row; i++){
            os << " ";
        }
        os << node->value << "\n";
        print(os, node->left, row+1);
        print(os,node->right, row+1);
        return os;
    }     // функция для вывода

    int countEven(Node* node){
        if(!node)
            return 0;
        return countEven(node->left) + countEven(node->right) + (node->value %2 == 0 ? 1 : 0);
    }

    bool arePositives(Node* node){
        if(!node)
            return true;
        return node->value > 0 && arePositives(node->left) && arePositives(node->right);
    }

    bool removeLeaves(Node* node){
        if(!node)
            return true;
        if(!(node->left || node->right)) {
            delete node;
            return true;
        } else {
            if (removeLeaves(node->left))
                node->left = nullptr;
            if (removeLeaves(node->right))
                node->right = nullptr;
            return false;
        }
    }

    void average(Node* node, int &sum, int &count){
        if (!node)
            return;
        sum+=node->value;
        count++;
        average(node->left,sum,count);
        average(node->right,sum,count);
    }

    bool find(std::vector<char> &accum, Node *node, int elem) {
        if (!accum.empty() && accum[accum.size() - 1] == 't')
            return true;
        if (!node)
            return false;
        if (node->value == elem) {
            accum.push_back('t');
            return true;
        }
        accum.push_back('0');
        if (!find(accum, node->left, elem)) {
            accum.pop_back();
            accum.push_back('1');
            if (!find(accum, node->right, elem)) {
                accum.pop_back();
                return false;
            }
        }
        return true;
    }
};


#endif //OMCPPUSHKA_BINARYTREE_H